package database

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/jcobhams/asari/document"
	"gitlab.com/jcobhams/asari/queryfilter"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"reflect"
	"time"
)

type client struct {
	Connection *mongo.Database
	Paginator  Paginator
}

var (
	Instance *client
)

func Init(mongoDSN, databaseName string) *client {
	ctx, _ := context.WithTimeout(context.Background(), time.Second*10)
	mClient, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoDSN))

	if err != nil {
		log.Panic(fmt.Sprintf("Could Not Connect To Server - %v", mongoDSN))
	}

	err = mClient.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Panic("Could Not Connect To Primary shard")
	}
	database := mClient.Database(databaseName)

	return &client{Connection: database}
}

// FindOne searches for a single document that matches the provided filters.
// If projection is nil, all fields are returned.
// To specify only select fields, use a bson.M - eg: bson.M{"email":1, "phone":1}
// Target has to be a pointer to a struct where the document will be unmarshalled into.
func (c *client) FindOne(collection string, filters []bson.E, projection, target interface{}) error {
	if reflect.TypeOf(target).Kind() != reflect.Ptr {
		return errors.New("asari: doc must be a pointer to a document")
	}
	opts := &options.FindOneOptions{
		Projection: projection,
	}

	filters = c.applyIsDeletedFilter(filters)

	return c.Connection.Collection(collection).FindOne(context.TODO(), filters, opts).Decode(target)
}

// FindOneByID finds a document that matches the provided ID in the collection.
// If projection is nil, all fields are returned.
// To specify only select fields, use a bson.M - eg: bson.M{"email":1, "phone":1}
// Target has to be a pointer to a struct where the document will be unmarshalled into.
func (c *client) FindOneByID(collection string, id primitive.ObjectID, projection, target interface{}) error {
	if reflect.TypeOf(target).Kind() != reflect.Ptr {
		return errors.New("asari: doc must be a pointer to a document")
	}
	opts := &options.FindOneOptions{
		Projection: projection,
	}

	filters := queryfilter.New().AddFilter(bson.E{Key: "_id", Value: id}).GetFilters()
	return c.Connection.Collection(collection).FindOne(context.TODO(), filters, opts).Decode(target)
}

// FindPaginated searches for document that matches the provided filters.
// PageOpts control CurrentPage and PerPage value.
// If projection is nil, all fields are returned.
// To specify only select fields, use a bson.M - eg: bson.M{"email":1, "phone":1}
// paginatorSort should be a bson.D - eg: bson.D{bson.E{Key: "_id", Value: -1}, bson.E{Key: "another, Value: "value"}}
func (c *client) FindPaginated(pageOptions PageOpts, collection string, filters []bson.E, projection interface{}, sort bson.D) (*PaginatedResult, error) {
	if sort == nil {
		sort = bson.D{bson.E{Key: "_id", Value: -1}}
	}

	filters = c.applyIsDeletedFilter(filters)

	paginator := NewPaginator(pageOptions)
	paginator.SetOffset()
	opts := &options.FindOptions{
		Projection: projection,
		Skip:       &paginator.Offset,
		Limit:      &paginator.PerPage,
		Sort:       sort,
	}

	totalRows, err := c.Connection.Collection(collection).CountDocuments(nil, filters)
	if err != nil {
		return nil, err
	}
	paginator.TotalRows = totalRows

	cur, err := c.Connection.Collection(collection).Find(context.TODO(), filters, opts)
	if err != nil {
		return nil, err
	}
	defer cur.Close(nil)
	var docs []bson.Raw
	for cur.Next(nil) {
		var doc *bson.Raw
		if err := cur.Decode(&doc); err == nil {
			docs = append(docs, *doc)
		}
	}

	paginator.SetTotalPages()
	paginator.SetPrevPage()
	paginator.SetNextPage()
	return &PaginatedResult{Results: docs, Paginator: *paginator}, nil
}

// FindLast returns the most recent document in the collection that matches the provided filters.
// It sorts based on the mongo objectId
func (c *client) FindLast(collection string, filters []bson.E, projection, target interface{}) error {
	if reflect.TypeOf(target).Kind() != reflect.Ptr {
		return errors.New("asari: target must be a pointer")
	}

	cur, err := c.findLast(collection, 1, filters, projection)
	if err != nil {
		return err
	}

	for cur.Next(nil) {
		cur.Decode(target)
	}
	return nil
}

// FindLastN returns the N (limit) most recent documents in the collection that matches the provided filters.
// It sorts based on provided mongo objectId
func (c *client) FindLastN(collection string, limit int, filters []bson.E, projection interface{}) (*mongo.Cursor, error) {
	return c.findLast(collection, limit, filters, projection)
}

func (c *client) findLast(collection string, limit int, filters []bson.E, projection interface{}) (*mongo.Cursor, error) {
	sort := bson.D{bson.E{Key: "_id", Value: -1}}

	filters = c.applyIsDeletedFilter(filters)

	opts := &options.FindOptions{
		Projection: projection,
		Sort:       sort,
	}
	opts.SetLimit(int64(limit))

	return c.Connection.Collection(collection).Find(context.TODO(), filters, opts)
}

func (c *client) applyIsDeletedFilter(filters []bson.E) []bson.E {
	//Extra Redundancy Incase is_deleted is not provided, default to false
	containsIsDeleted := false
	for _, v := range filters {
		if v.Key == "is_deleted" {
			containsIsDeleted = true
			break
		}
	}
	if !containsIsDeleted {
		filters = append(filters, bson.E{Key: "is_deleted", Value: false})
	}

	return filters
}

// FindAll - returns a list of all the document that match the filter or returns an error.
// To be used with care as a lot of document could be returned and use up a lot of memory.
func (c *client) FindAll(collection string, filters []bson.E, projection interface{}, sort bson.D) (*mongo.Cursor, error) {
	if sort == nil {
		sort = bson.D{bson.E{Key: "_id", Value: -1}}
	}

	filters = c.applyIsDeletedFilter(filters)

	opts := &options.FindOptions{
		Projection: projection,
		Sort:       sort,
	}

	return c.Connection.Collection(collection).Find(context.TODO(), filters, opts)
}

func (c *client) updateDocument(collection string, filters []bson.E, doc interface{}) (*mongo.SingleResult, error) {
	doc.(document.Document).BeforeUpdate()
	result := c.Connection.Collection(collection).FindOneAndReplace(context.TODO(), filters, doc)
	if result.Err() != nil {
		return nil, errors.New(result.Err().Error())
	}
	return result, nil
}

func (c *client) SaveDocument(collection string, doc interface{}) (interface{}, error) {
	if reflect.TypeOf(doc).Kind() != reflect.Ptr {
		return nil, errors.New("asari: doc must be a pointer to a document")
	}

	if !doc.(document.Document).CanSave() {
		return nil, errors.New("cannot save new document. call document.Setup() before calling SaveDocument()")
	}

	if doc.(document.Document).IsNew() {
		_, err := c.Connection.Collection(collection).InsertOne(context.TODO(), doc)
		if err == nil {
			doc.(document.Document).SetIsNew(false)
		}
		return doc, err
	} else {
		qf := queryfilter.New().AddFilter(bson.E{Key: "_id", Value: doc.(document.Document).GetID()}).GetFilters()
		_, err := c.updateDocument(collection, qf, doc)
		return doc, err
	}
}

func (c *client) SoftDeleteDocument(collection string, doc interface{}) (*mongo.SingleResult, error) {
	if reflect.TypeOf(doc).Kind() != reflect.Ptr {
		return nil, errors.New("asari: doc must be a pointer to a document")
	}

	d := doc.(document.Document)
	d.BeforeSoftDelete()
	id := d.GetID()

	qf := queryfilter.New().AddFilter(bson.E{Key: "_id", Value: id}).GetFilters()
	return c.updateDocument(collection, qf, doc)
}

func (c *client) CountDocuments(collection string, filters interface{}) (int, error) {
	count, err := c.Connection.Collection(collection).CountDocuments(context.TODO(), filters)
	return int(count), err
}

func (c *client) HardDeleteDocument(collection string, doc interface{}) (*mongo.DeleteResult, error) {
	if reflect.TypeOf(doc).Kind() != reflect.Ptr {
		return nil, errors.New("asari: doc must be a pointer to a document")
	}

	qf := queryfilter.New().
		AddFilter(bson.E{Key: "_id", Value: doc.(document.Document).GetID()}).
		GetFilters()
	return c.Connection.Collection(collection).DeleteOne(context.TODO(), qf)
}
